# rugutils: a suite of minimalist Carpet data reading and plotting scripts
# Copyright (C) 2016, Federico Guercilena
# <guercilena@th.physik.uni-frankfurt.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import matplotlib.pyplot as plt
from read_hdf5 import read_file_structure, read_component
import matplotlib.gridspec as gs
import expression_evaluator as ee
import h5py
from os import devnull
import sys
from utils import parse_dataset_name, compute_reflevels_extent
from utils import GridInterpolator, GridInterpolationError
from copy import copy


documentation = """
The window command lets you plot 2D Carpet data in HDF5 format at a given
iteration. It also displays the data value at the current cursor position.

'DATASET'
One strings of the type "varname@datafile", where:

    varname: The name of the desired variable

    datafile: A path to one Carpet 2D HDF5 files.

The variable name is optional. In that case, the first variable found in the
file displayed. Also, the variable name doesn't have to match exactly the one
in the file (which could be very long): a string appearing in the variable
name and uniquely identifying it is sufficient.

'ITERATION'
Integer, required. The iteration number to read and display.

'-R/--reflevel REFLEVEL'
If this option is present, only the refinement level specified in it will be
read and displayed.

'-f/--expression EXPR'
This option allows the user to apply some mathematical expression to the data
before plotting. The expression EXPR will be evaluated at runtime and the
result will be applied to all input DATAFILES. In the EXPRESSION, 'z'
represents the values of the data on the ordinate (or z) axis. So for example:
    -f "z*3 + abs(z)"
will plot every data point multiplied by 3 and add its absolute value.
Common uses of the option are taking the log or the absolute value of the data,
but many more functions and expressions are supported, including array
indexing. For details and a list of functions available, type "rug doc
expression_evaluator".

'--limits VMIN VMAX'
The minimum and maximum values of the colorscale.

'--bounds XMIN XMAX YMIN YMAX'
Extent of the axes

'--colormap'
Use the given colormap

'-e'
Plot the grid as well.

[Symmetry options]
'-p'
Plot also central/pi-symmetric data.
'-x'
Plot also x-symmetric data
'-y'
Plot also y-symmetric data
'-xy'
Plot also x- and y- symmetric data.

'--save-to FILENAME'
Don't display the figure, but directly save it to a rasterized file
called FILENAME, with resolution of 1280x720 at 96 dpi.

'--fig-size'
Two ints specifying the figure size in pixels along the x and y axis
respectively. Only relevant and available when saving to disk (with the
'--save-to' option). The default is 1280x720.

'-q/--quiet'
Disable output messages.
"""

__doc__ = documentation


class PointValue:

    def __init__(self, data):
        self.interpolator = GridInterpolator(data)

    def __call__(self, x, y):
        try:
            z = self.interpolator(x, y)
            return 'x={:.2f}, y={:.2f}, z={:.2f}'.format(x, y, z)
        except GridInterpolationError:
            return 'x={:.2f}, y={:.2f}, z=N/A'.format(x, y)


def window(dataset, iteration=None, reflevel=None, edges=False,
           saveto=None, limits=None, cmap="viridis",
           pisym=False, xsym=False, ysym=False, xysym=False,
           bounds=None, expr=None, quiet=False, figsize=None):

    # Disable messages
    if quiet:
        sys.stdout = open(devnull, 'w')

    # Set up the expression evaluator if needed
    if expr is not None:
        evaluator = ee.SimpleEval()

    # Set up the figure and axes
    fig = plt.figure()

    fig.set_dpi(96)
    if figsize is not None:
        fig.set_size_inches(figsize[0]/96., figsize[1]/96.)
    else:
        fig.set_size_inches(1280./96., 720./96.)

    grid = gs.GridSpec(1, 2, width_ratios=[10, 1])

    rasterized = False
    if saveto is not None:
        rasterized = True

    ax = fig.add_subplot(grid[0, 0], rasterized=rasterized)
    ax.set_aspect('equal')
    cbar_ax = fig.add_subplot(grid[0, 1], rasterized=rasterized)

    # Set the color of the cell edges
    edgecolors = "face"
    if edges:
        edgecolors = "black"

    # Set the bounds on the axes
    if bounds is not None:
        ax.set_xlim([bounds[0], bounds[1]])
        ax.set_ylim([bounds[2], bounds[3]])

    # Set the max and min values
    if limits is not None:
        vmin, vmax = limits
    else:
        vmin, vmax = None, None

    # Get the variable and file names
    name, file = parse_dataset_name(dataset)
    name = name[0]

    # Create the h5py file object (read only)
    hfile = h5py.File(file, 'r')

    # Read the structure of the file
    realname, struct = read_file_structure(hfile, name)

    # Check the wanted iteration is available
    if iteration not in struct:
        print("Error! Iteration", iteration, "not in datafile!"
              " Aborting...", flush=True)
        sys.exit()

    # Select the wanted iteration
    struct = struct[iteration]

    # Set up a copy of the data
    datacopy = copy(struct)

    if reflevel is not None:

        # Check the wanted reflevel is available
        if reflevel not in struct:
            print("Error! Refinement level", reflevel, "not present at"
                  " iteration", iteration, "! Aborting...", flush=True)
            sys.exit()

        # Select the wanted reflevel
        reflevel_list = [reflevel]

        # Remove the unneeded levels from the data copy
        for rl in list(datacopy):
            if rl != reflevel:
                datacopy.pop(rl)

    else:

        # Select all reflevels
        reflevel_list = struct

    # Loop over the refinement levels
    for rl in reflevel_list:

        # Loop over the components
        for c in struct[rl]:

            # Read in the component
            comp = read_component(hfile, realname,
                                  iteration, rl, c)

            # Save the component
            datacopy[rl][datacopy[rl].index(c)] = comp

            # Compute the grid
            x, y = comp.get_grid()

            # Apply an expression on the data, if needed
            if expr is not None:
                evaluator.names["z"] = comp.data
                comp.data = evaluator.eval(expr)

            # Plot the data
            im = ax.pcolormesh(x, y, comp.data, cmap=cmap,
                               vmin=vmin, vmax=vmax,
                               edgecolors=edgecolors, lw=0.05)

            # Plot the symmetric data
            if pisym:
                ax.pcolormesh(-x, -y, comp.data, cmap=cmap,
                              vmin=vmin, vmax=vmax,
                              edgecolors=edgecolors, lw=0.05)
            if xsym:
                ax.pcolormesh(-x,  y, comp.data, cmap=cmap,
                              vmin=vmin, vmax=vmax,
                              edgecolors=edgecolors, lw=0.05)
            if ysym:
                ax.pcolormesh(x, -y, comp.data, cmap=cmap,
                              vmin=vmin, vmax=vmax,
                              edgecolors=edgecolors, lw=0.05)
            if xysym:
                ax.pcolormesh(-x,  y, comp.data, cmap=cmap,
                              vmin=vmin, vmax=vmax,
                              edgecolors=edgecolors, lw=0.05)
                ax.pcolormesh(x, -y, comp.data, cmap=cmap,
                              vmin=vmin, vmax=vmax,
                              edgecolors=edgecolors, lw=0.05)
                ax.pcolormesh(-x, -y, comp.data, cmap=cmap,
                              vmin=vmin, vmax=vmax,
                              edgecolors=edgecolors, lw=0.05)

    # Set up the colorbar
    if limits is not None:
        fig.colorbar(im, cax=cbar_ax, spacing="uniform")
    else:
        cbar_ax.set_xlim([0, 1])
        cbar_ax.set_ylim([0, 1])
        cbar_ax.set_yticks([])
        cbar_ax.set_xticks([])
        cbar_ax.text(0.5, 0.5, "No vmin/vmax provided",
                     va="center", ha="center",
                     rotation="vertical", size="xx-large")

    # Set up the ticks and title
    ax.tick_params(which="major", reset=True, labelsize="x-large")
    ax.set_title((name if name else "data") + "@" + file
                 + " at time: " + str(comp.time), fontsize="x-large")

    # Get the extents of the levels
    datacopy = compute_reflevels_extent(datacopy)

    # Show the plot or save it
    if saveto is not None:
        print("Saving to file...", end="", flush=True)
        fig.savefig(saveto, dpi=96)
        print(" Done.", flush=True)
        sys.exit()

    else:
        ax.format_coord = PointValue(datacopy)
        plt.show()
