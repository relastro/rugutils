# rugutils: a suite of minimalist Carpet data reading and plotting scripts
# Copyright (C) 2016, Federico Guercilena
# <guercilena@th.physik.uni-frankfurt.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import numpy as np
import matplotlib.pyplot as plt
from read_hdf5 import read_file_structure, read_component
from matplotlib import animation
import matplotlib.gridspec as gs
import h5py
from itertools import repeat, cycle
import sys
import expression_evaluator as ee
from os import devnull
from utils import parse_dataset_name


documentation = """
The cinema command is a lets you animate 2D HDF5 Carpet data.

'DATASET'
One or more strings of the type "varname@datafile", where:

    varname: The name of the desired variable

    datafile: A path to one Carpet 2D HDF5 files.

The variable name is optional. In that case, the first variable found in the
file displayed. Also, the variable name doesn't have to match exactly the one
in the file (which could be very long): a string appearing in the variable
name and uniquely identifying it is sufficient.

If more then one file is given, they will be chained together on the time
axis, removing any duplicate iterations, but only the variable given for
each file has to be the same.

'-R/--reflevel REFLEVEL'
If this option is present, only the refinement level specified in it will be
read and displayed.

'-f/--expression EXPR'
This option allows the user to apply some mathematical expression to the data
before plotting. The expression EXPR will be evaluated at runtime and the
result will be applied to all input DATAFILES. In the EXPRESSION, 'z'
represents the values of the data on the ordinate (or z) axis. So for example:
    -f "z*3 + abs(z)"
will plot every data point multiplied by 3 and add its absolute value.
Common uses of the option are taking the log or the absolute value of the data,
but many more functions and expressions are supported, including array
indexing. For details and a list of functions available, type "rug doc
expression_evaluator".

'--limits VMIN VMAX'
The minimum and maximum values of the colorscale.

'--bounds XMIN XMAX YMIN YMAX'
Extent of the axes

'-r/--rate RATE'
The frame rate, i.e. the number of frames per second.

'--colormap'
Use the given colormap

'-e'
Plot the grid as well.

'--save-to FILENAME'
Don't display the figure, but directly save it to a rasterized file
called FILENAME, with resolution of 1280x720 at 96 dpi.

'--fig-size'
Two ints specifying the figure size in pixels along the x and y axis
respectively. Only relevant and available when saving to disk (with the
'--save-to' option). The default is 1280x720.

'-q/--quiet'
Disable output messages.

[KEYBOARD COMMANDS]
'spacebar'
Pause and resume the animation

'right/left arrows'
Skip ahead or behind in the animation by one single frame.
This works also when the animation is paused.

'up/down arrows'
Skip ahead or behind in the animation. The amount of time skipped is
hard coded to 1/20 of the total data length. This works also when
the animation is paused.

'ctrl+r'
Restart the animation, from the very first frame. If the animation
is paused, it will stay so.

'ctrl+e'
Skip to the end of the animation (last frame) and pause the animation
there.

'q'
Quit the program.
"""

__doc__ = documentation


def cinema(datasets, reflevel=None, saveto=None,
           figsize=None, limits=None,
           cmap="viridis", expr=None, rate=10,
           edges=False, quiet=False, bounds=None):

    # Disable messages
    if quiet:
        sys.stdout = open(devnull, 'w')

    # Pause between frames in ms
    interval = 1000/rate

    # Set up the expression evaluator if needed
    if expr is not None:
        evaluator = ee.SimpleEval()

    # Setup the figure, gridspec and axes instances
    fig = plt.figure()
    grid = gs.GridSpec(2, 2, width_ratios=[10, 1], height_ratios=[1, 20])

    rasterized = False
    if saveto is not None:
        rasterized = True

    ax = fig.add_subplot(grid[1, 0], rasterized=rasterized)
    ax.set_aspect('equal')
    cbar_ax = fig.add_subplot(grid[1, 1], rasterized=rasterized)

    fig.set_dpi(96)
    if figsize is not None:
        fig.set_size_inches(figsize[0]/96., figsize[1]/96.)
    else:
        fig.set_size_inches(1280./96., 720./96.)

    # Set the color of the cell edges
    edgecolors = "face"
    if edges:
        edgecolors = "black"

    # Set the bounds on the axes
    if bounds is not None:
        ax.set_xlim([bounds[0], bounds[1]])
        ax.set_ylim([bounds[2], bounds[3]])

    # Set the max and min values
    if limits is not None:
        vmin, vmax = limits
    else:
        vmin, vmax = None, None

    # Get the names, check they are the same
    names = []
    for dataset in datasets:
        name, file = parse_dataset_name(dataset)
        names = names + name
    names = [name == names[0] for name in names]
    if not np.all(names):
        print("Error! All variable names must be the same! Aborting...")
        sys.exit()

    i = 0
    struct = {}
    hfiles = {}
    # Loop over datasets
    for dataset in datasets:

        # Alert the user
        print("Scanning file", i + 1, "of", len(datasets), "...", end=" ",
              flush=True)

        # Get the names and file
        name, file = parse_dataset_name(dataset)
        name = name[0]

        # Create the h5py file object (read only)
        hfile = h5py.File(file, 'r')

        # Read the structure of the file
        realname, tmp = read_file_structure(hfile, name)

        # Store the file and update the structure
        hfiles[(sorted(tmp)[0], sorted(tmp)[-1])] = hfile
        struct.update(tmp)

        # Alert the user
        print("Done.", flush=True)
        i += 1

    # Get the iterations
    iterations = sorted(struct)

    # Adjust the files
    # TODO

    # Loop over iterations
    times = [[]]*len(struct)
    ncomp = [[]]*len(struct)
    j = 0
    for iteration in iterations:

        for key in hfiles:
            if key[0] <= iteration <= key[1]:
                hfile = hfiles[key]

        if reflevel is not None:

            # Check the wanted reflevel is available
            if reflevel not in struct[iteration]:
                print("Error! Refinement level", reflevel,
                      "not present at iteration", iteration,
                      "! Aborting...", flush=True)
                sys.exit()

            # Select reflevel
            a_reflevel = reflevel
            reflevel_list = [reflevel]

        else:

            # Select the coarsest
            a_reflevel = 0

            # Select all
            reflevel_list = struct[iteration]

        ncomp[j] = 0
        for rl in reflevel_list:
            ncomp[j] += len(struct[iteration][rl])

        times[j] = read_component(hfile, realname,
                                  iteration, a_reflevel,
                                  struct[iteration][a_reflevel]
                                  [0],
                                  metadata_only=True).time
        j += 1

    # Get the initial times, final times, delta time
    t_initial = times[0]
    t_final = times[-1]
    delta_t = times[1] - t_initial

    # Get the maximum number of components
    max_comp = np.max(ncomp)

    # Create an array of plots
    ims = np.empty(max_comp, dtype=object)
    ims[0] = ax.pcolormesh(np.empty([0, 0]),
                           vmin=vmin, vmax=vmax,
                           cmap=cmap)

    print("Setting up the animation object...", flush=True)

    # Get the extent in time
    min_t = np.min(t_initial)
    max_t = np.max(t_final)

    # Get the datafile with the finest resolution
    dt = np.min(delta_t)
    # finest_data_idx = np.where(delta_ts == dt)[0][0]

    # Create the progress bar
    pbar_ax = fig.add_subplot(grid[0, 0], xlim=[min_t, max_t], ylim=[0, 1],
                              rasterized=rasterized)
    pbar_ax.set_yticks([])
    time = pbar_ax.text((max_t - min_t)/2 + min_t, 0.9,
                        "Time: {0:0f}".format(min_t),
                        va="top", ha="center", color="black", size="xx-large")
    time_line = pbar_ax.barh(0.02, 0, height=0.3, color="black")[0]

    # Get the total number of frames
    max_frames = int((max_t - min_t)/dt)

    # Set up the colorbar
    if limits is not None:
        fig.colorbar(ims[0], cax=cbar_ax, spacing="uniform")
    else:
        cbar_ax.set_xlim([0, 1])
        cbar_ax.set_ylim([0, 1])
        cbar_ax.set_yticks([])
        cbar_ax.set_xticks([])
        cbar_ax.text(0.5, 0.5, "No vmin or vmax provided",
                     va="center", ha="center",
                     rotation="vertical", size="xx-large")

    # Set up the title
    pbar_ax.set_title(name if name else "data", size="xx-large")

    # The "empty" init function, needed for bliting
    def init():
        ims[0] = ax.pcolormesh(np.empty([0, 0]), cmap=cmap,
                               vmin=vmin, vmax=vmax)

        # Set the bounds on the axes
        if bounds is not None:
            ax.set_xlim([bounds[0], bounds[1]])
            ax.set_ylim([bounds[2], bounds[3]])

        time.set_text("Time: {0:0f}".format(0.0))
        time_line.set_width(0)
        return [ims[0], time, time_line]

    # The animation function (draws a frame)
    def animate(n):
        now = min_t + dt*n

        # Figure out the iteration number
        iteration = iterations[n]

        for key in hfiles:
            if key[0] <= iteration <= key[1]:
                hfile = hfiles[key]

        # Loop over the refinement levels
        if reflevel is not None:
            reflevel_list = [reflevel]
        else:
            reflevel_list = struct[iteration]
        j = 0
        for rl in reflevel_list:

            # Loop over the components
            for c in struct[iteration][rl]:

                # Read in the component
                comp = read_component(hfile, realname,
                                      iteration, rl, c)

                # Compute the grid points
                x, y = comp.get_grid()

                # Apply an expression on the data, if needed
                if expr is not None:
                    evaluator.names["z"] = comp.data
                    comp.data = evaluator.eval(expr)

                # Plot the data
                ims[j] = ax.pcolormesh(x, y, comp.data, cmap=cmap,
                                       vmin=vmin, vmax=vmax,
                                       edgecolors=edgecolors, lw=0.05)
                j += 1

            # "Empty" the remaining plots
            ims[j:max_comp] = [[]]*(max_comp - j)

        # Set the bounds on the axes
        if bounds is not None:
            ax.set_xlim([bounds[0], bounds[1]])
            ax.set_ylim([bounds[2], bounds[3]])

        time.set_text("Time: {0:0f}".format(now))
        time_line.set_width(now)

        if saveto is not None:
            print("Done frame ", n + 1, "of", max_frames, flush=True)

        return list(filter(bool, ims)) + [time, time_line]

    anim = animation.FuncAnimation(fig, animate, init_func=init,
                                   frames=max_frames, interval=interval,
                                   blit=True, repeat=False)
    if saveto is not None:
        print("Saving to file...", end="", flush=True)
        anim.save(saveto, writer="ffmpeg", fps=rate, dpi=96,
                  codec="libx264", bitrate=400)
        print(" Done.", flush=True)
        sys.exit()

    print(" Done.", flush=True)

    # Pause, quit, restart, etc.,  the animation on key events (ugly)
    piece = max_frames//20
    pause = cycle([True, False])

    def onKey(event):
        if event.key == "q":
            sys.exit()
        elif event.key == " ":
            if pause.__next__():
                anim.frame_seq = repeat(next(anim.frame_seq, max_frames) - 1)
            else:
                anim.frame_seq = range(next(anim.frame_seq, max_frames),
                                       max_frames).__iter__()
        elif event.key == "up":
            if pause.__next__():
                anim.frame_seq = range(anim.frame_seq.__next__() + piece,
                                       max_frames).__iter__()
            else:
                anim.frame_seq = repeat(anim.frame_seq.__next__() + piece)
            pause.__next__()
        elif event.key == "down":
            if pause.__next__():
                anim.frame_seq = range(anim.frame_seq.__next__() - piece,
                                       max_frames).__iter__()
            else:
                anim.frame_seq = repeat(anim.frame_seq.__next__() - piece)
            pause.__next__()
        elif event.key == "right":
            if pause.__next__():
                anim.frame_seq = range(anim.frame_seq.__next__() + 1,
                                       max_frames).__iter__()
            else:
                anim.frame_seq = repeat(anim.frame_seq.__next__() + 1)
            pause.__next__()
        elif event.key == "left":
            if pause.__next__():
                anim.frame_seq = range(anim.frame_seq.__next__() - 1,
                                       max_frames).__iter__()
            else:
                anim.frame_seq = repeat(anim.frame_seq.__next__() - 1)
            pause.__next__()
        elif event.key == "ctrl+r":
            if pause.__next__():
                anim.frame_seq = range(0, max_frames).__iter__()
            else:
                anim.frame_seq = repeat(0)
            pause.__next__()
        elif event.key == "ctrl+e":
            anim.frame_seq = repeat(max_frames - 1)
            if not pause.__next__():
                pause.__next__()

    fig.canvas.mpl_connect('key_press_event', onKey)

    # Show the animation
    plt.show(anim)
