from re import search, compile
import numpy as np


def parse_column_names(lines):
    """Given a list of lines read from a Carpet ASCII file, figure out how
    many data columns there are, which are their names and numbering. Return
    this information in a dictionary.

    PARAMETERS:
    ------------

    lines: (list of strings) some lines read from the top of a Carpet ASCII
           file.

    RETURNS:
    ---------

    column_names: a dictionary. The keys of the dictionary are the names of
                  the data columns available in the file. The corresponding
                  items are the column numbers (zero-based).
    """

    # Define the matchstring
    match_string = compile(r"([0-9]{1,2}:[^0-9].+ ?)+")

    # Match it through the given lines and remove non-matches
    matches = np.array([search(match_string, line) for line in lines])
    matches = matches[matches.nonzero()]

    columns = []
    # For every match, split the various colums
    for match in matches:
        columns = columns + match.group(1).split()

    column_names = {}
    # Regroup everything nicely in a dictionary
    for column in columns:
        number, name = column.split(':')
        number = int(number) - 1  # Python is 0-based
        column_names[name] = number

    return column_names


def parse_dataset_name(name):
    """Separate a dataset name in its file name and variable name(s).

    PARAMETERS:
    ------------

    name: (string) the dataset string.

    RETURNS:
    ---------

    vars, file: a tuple. The first element ('vars') is a list of variable
                names, or None if no variable was specified. The second
                ('file') is the filename.
    """

    if '@' in name:
        vars, file = name.split('@')
        vars = vars.split(',')
    else:
        vars, file = [""], name

    return vars, file


def compute_reflevels_extent(data):
    """Given a 'struct' containing the data of a single iteration of a 2D
    HDF5 file, computes the extents of the single reflevels and return the
    'struct' modified to contain this data as well.

    PARAMETERS:
    ------------

    data: (dict) a 2D HDF5 file struct.

    RETURNS:
    ---------

    data: same as the input, except now every reflevel is a 2 elements tuple.
          The second element is the same as the original data, the first one
          is a class with four attributes (low_x, low_y, high_x, high_y)
          representing the extent of the reflevel.
    """

    class extents:
        def __init__(self, low_x, low_y, high_x, high_y):
            self.low_x = low_x
            self.low_y = low_y
            self.high_x = high_x
            self.high_y = high_y

    for rl in data:
        low_x = np.min([c.origin[0] for c in data[rl]])
        low_y = np.min([c.origin[1] for c in data[rl]])

        high_x = np.max([c.end[0] for c in data[rl]])
        high_y = np.max([c.end[1] for c in data[rl]])

        data[rl] = extents(low_x, low_y, high_x, high_y), data[rl]

    return data


class GridInterpolationError(Exception):
    def __init__(self, x, y):
        self.message = "Interpolation error at "
        "(x={0:.2f}, y={1:.2f})".format(x, y)


class GridInterpolator:

    def __init__(self, data):
        self.data = data
        self.reflevels = sorted(data.keys())[::-1]

    def __call__(self, x, y):
        z = None
        for rl in self.reflevels:
            low_x = self.data[rl][0].low_x
            low_y = self.data[rl][0].low_y
            high_x = self.data[rl][0].high_x
            high_y = self.data[rl][0].high_y
            if ((low_x <= x <= high_x) and (low_y <= y <= high_y)):
                for c in self.data[rl][1]:
                    low_x = c.origin[0]
                    low_y = c.origin[1]
                    high_x = c.end[0]
                    high_y = c.end[1]
                    if (low_x <= x <= high_x) and (low_y <= y <= high_y):
                        i = int((x - low_x)/c.delta[0])
                        j = int((y - low_y)/c.delta[1])
                        z = c.data[j, i]
                        break
            if z is not None:
                break

        if z is None:
            raise GridInterpolationError(x, y)
        else:
            return z
