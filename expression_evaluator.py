# rugutils: a suite of minimalist Carpet data reading and plotting scripts
# Copyright (C) 2016, Federico Guercilena
# <guercilena@th.physik.uni-frankfurt.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# The following code is adapted from one written by
# Daniel Fairhead, available at https://github.com/danthedeckie/simpleeval
# Read on for more details.


# SimpleEval - (C) 2013/2015 Daniel Fairhead
# -------------------------------------
# Designed for things like in a website where you want to allow the user to
# generate a string, or a number from some other input, without allowing full
# eval() or other unsafe or needlessly complex linguistics.
#
# Initial idea copied from J.F. Sebastian on Stack Overflow
# ( http://stackoverflow.com/a/9558001/1973500 ) with
# modifications and many improvments.
#
# -------------------------------------
# Contributors:
# - corro (Robin Baumgartner) (py3k)
# - dratchkov (David R) (nested dicts)
# - marky1991 (Mark Young) (slicing)
# - T045T (Nils Berg) (!=, py3kstr, obj.attributes)
#
# -------------------------------------

import ast
import operator as op
import numpy as np
from sys import exit
from functools import partial


documentation = """
This module provides utilities to evaluate an expression stored in a string at
runtime. It supports many mathematical functions:
    +     : additionst.Add
    -     : subtraction and opposite
    *     : multiplication
    /     : float division
    **    : exponentiation
    ==, !=, >, >=, <, <= : comparison
    abs   : absolute value
    sqrt  : square root
    log, log2, log10 : logarithm in base e, 2 and 10
    exp   : exponential
    floor : floor function
    ceil  : ceiling function
    clip  : clip values between two extrema
    real  : real part
    imag  : imaginary part
    angle : argument of a complex number
    unwrap : numpy unwrap ufunc
    where : numpy where
    piecewise : modified numpy piecewise (supports different args to different funcs)
    _     : identity function
    pt    : polytrope, i.e. pt(k, gamma, rho) = k*rho**gamma
All (almost) of these functions or operators are implemented via their numpy
equivalents, therefore they operate transparently on numpy arrays in an
element wise fashion.

Array indexing ("[]" notation) is also supported, as well as lists.

The constants "True" and "False" are also available.
"""

__doc__ = documentation


def piecewise(arr, conds, funcs, args=None):

    if len(conds) != len(funcs):
        print("Error! Number of conditions must match number of functions! Aborting...")
        exit()

    if (args is not None) and (len(args) != len(funcs)):
        print("Error! Number of arguments must match number of functions! Aborting...")
        exit()

    if args is None:
        return np.piecewise(arr, conds, funcs)
    else:
        funcs = [partial(funcs[i], *args[i]) for i in range(len(funcs))]
        return np.piecewise(arr, conds, funcs)


def pt(k, gamma, arr):
    return k*arr**gamma

def merge_dicts(x, y):
    z = x.copy()
    z.update(y)
    return z


########################################
# Module wide 'globals'
#######################################

MAX_STRING_LENGTH = 100000

########################################
# Exceptions:
########################################


class InvalidExpression(Exception):
    pass


class FunctionNotDefined(InvalidExpression):
    def __init__(self, func_name, expression):
        self.message = "Function '{0}' not defined"
        "for expression '{1}'.".format(func_name, expression)
        self.func_name = func_name
        self.expression = expression
        super(InvalidExpression, self).__init__(self.message)


class NameNotDefined(InvalidExpression):
    def __init__(self, name, expression):
        self.message = "'{0}' is not defined"
        "for expression '{1}'".format(name, expression)
        self.name = name
        self.expression = expression
        super(InvalidExpression, self).__init__(self.message)


class AttributeDoesNotExist(InvalidExpression):
    def __init__(self, attr, expression):
        self.message = "Attribute '{0}' does not"
        "exist in expression '{1}'".format(attr, expression)
        self.attr = attr
        self.expression = expression


class FeatureNotAvailable(InvalidExpression):
    pass


class NumberTooHigh(InvalidExpression):
    pass


class StringTooLong(InvalidExpression):
    pass


########################################
# Defaults for the evaluator:
########################################

DEFAULT_OPERATORS = {ast.Add: op.add, ast.Sub: op.sub, ast.Mult: op.mul,
                     ast.Div: op.truediv, ast.Pow: op.pow, ast.Mod: op.mod,
                     ast.Eq: op.eq, ast.NotEq: op.ne, ast.Gt: op.gt,
                     ast.Lt: op.lt, ast.GtE: op.ge, ast.LtE: op.le,
                     ast.USub: op.neg, ast.UAdd: op.pos}

DEFAULT_FUNCTIONS = {"abs": np.abs, "sqrt": np.sqrt, "log": np.log,
                     "log10": np.log10, "log2": np.log2, "exp": np.exp,
                     "floor": np.floor, "ceil": np.ceil, "clip": np.clip,
                     "real": np.real, "imag": np.imag, "angle": np.angle,
                     "unwrap": np.unwrap, "piecewise": piecewise,
                     "list": list, "where": np.where, "_": (lambda x: x),
                     "pt": pt}

DEFAULT_NAMES = {"True": True, "False": False,
                 "abs": np.abs, "sqrt": np.sqrt, "log": np.log,
                 "log10": np.log10, "log2": np.log2, "exp": np.exp,
                 "floor": np.floor, "ceil": np.ceil, "clip": np.clip,
                 "real": np.real, "imag": np.imag, "angle": np.angle,
                 "unwrap": np.unwrap, "piecewise": piecewise,
                 "list": list, "where": np.where, "_": (lambda x: x),
                 "pt": pt}


########################################
# The actual evaluator:
########################################

class SimpleEval:
    """A very simple expression parser."""

    expr = ""

    def __init__(self, operators=None, functions=None, names=None, keep_defaults=False):
        """Create the evaluator instance.  Set up valid operators (+,-, etc)
           functions (add, random, get_val, whatever) and names.
           If "keep_defaults" is set, any function arguments will be added
           to the default/integrated operators/functions/names.
        """

        if not operators:
            operators = DEFAULT_OPERATORS
        if not functions:
            functions = DEFAULT_FUNCTIONS
        if not names:
            names = DEFAULT_NAMES

        self.operators = operators
        self.functions = functions
        self.names = names

        if keep_defaults and operators:
            self.operators = merge_dicts(self.operators, DEFAULT_OPERATORS)
        if keep_defaults and functions:
            self.functions = merge_dicts(self.functions, DEFAULT_FUNCTIONS)
        if keep_defaults and names:
            self.names = merge_dicts(self.names, DEFAULT_NAMES)

    def eval(self, expr):
        """Evaluate an expresssion, using the operators, functions and
           names previously set up."""

        # Set a copy of the expression aside, so we can give nice errors.

        self.expr = expr

        # Evaluate:
        return self._eval(ast.parse(expr).body[0].value)

    def _eval(self, node):
        """The internal eval function used on each node in the parsed tree."""

        if isinstance(node, ast.Num):
            return node.n
        elif isinstance(node, ast.Str):
            if len(node.s) > MAX_STRING_LENGTH:
                raise StringTooLong("String Literal in statement is too long!"
                                    " ({0}, when {1} is max)"
                                    .format(len(node.s), MAX_STRING_LENGTH))
            return node.s

        # python 3 compatibility:??

        elif (hasattr(ast, 'NameConstant') and
                isinstance(node, ast.NameConstant)):  # <bool>
            return node.value

        # operators, functions, etc:

        elif isinstance(node, ast.UnaryOp):  # - and + etc.
            return self.operators[type(node.op)](self._eval(node.operand))
        elif isinstance(node, ast.BinOp):  # <left> <operator> <right>
            return self.operators[type(node.op)](self._eval(node.left),
                                                 self._eval(node.right))
        elif isinstance(node, ast.BoolOp):  # and & or...
            if isinstance(node.op, ast.And):
                return all((self._eval(v) for v in node.values))
            elif isinstance(node.op, ast.Or):
                return any((self._eval(v) for v in node.values))
        elif isinstance(node, ast.Compare):  # 1 < 2, a == b...
            return self.operators[type(node.ops[0])](self._eval(node.left),
                                                     self._eval(
                                                         node.comparators[0]))
        elif isinstance(node, ast.IfExp):  # x if y else z
            return self._eval(node.body) if self._eval(node.test) \
                                         else self._eval(node.orelse)
        elif isinstance(node, ast.Call):  # function...
            try:
                return self.functions[node.func.id](*(self._eval(a)
                                                      for a in node.args))
            except KeyError:
                raise FunctionNotDefined(node.func.id, self.expr)

        # variables/names:

        elif isinstance(node, ast.Name):  # a, b, c...
            try:
                # This happens at least for slicing
                # This is a safe thing to do because it is impossible
                # that there is a true exression assigning to none
                # (the compiler rejects it, so you can't even pass
                # that to ast.parse)
                if node.id == "None":
                    return None
                elif isinstance(self.names, dict):
                    return self.names[node.id]
                elif callable(self.names):
                    return self.names(node)
                else:
                    raise InvalidExpression("Trying to use name (variable)"
                                            "'{0}' when no 'names' defined for"
                                            " evaluator".format(node.id))

            except KeyError:
                raise NameNotDefined(node.id, self.expr)

        elif isinstance(node, ast.Subscript):  # b[1]
            return self._eval(node.value)[self._eval(node.slice)]

        elif isinstance(node, ast.Attribute):  # a.b.c
            try:
                return self._eval(node.value)[node.attr]
            except (KeyError, TypeError):
                pass

            # Maybe the base object is an actual object, not just a dict
            try:
                return getattr(self._eval(node.value), node.attr)
            except (AttributeError, TypeError):
                pass

            # If it is neither, raise an exception
            raise AttributeDoesNotExist(node.attr, self.expr)

        elif isinstance(node, ast.Index):
            return self._eval(node.value)
        elif isinstance(node, ast.Slice):
            lower = upper = step = None
            if node.lower is not None:
                lower = self._eval(node.lower)
            if node.upper is not None:
                upper = self._eval(node.upper)
            if node.step is not None:
                step = self._eval(node.step)
            return slice(lower, upper, step)

        elif isinstance(node, ast.List):
            return [self._eval(i) for i in node.elts]
        else:
            raise FeatureNotAvailable("Sorry, {0} is not available in this"
                                      " evaluator".format(type(node).
                                                          __name__))
