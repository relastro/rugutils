# rugutils: a suite of minimalist Carpet data reading and plotting scripts
# Copyright (C) 2016, Federico Guercilena
# <guercilena@th.physik.uni-frankfurt.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import numpy as np
import csv
from sys import exit
from utils import parse_column_names


def read_0D_data(datafile, columns=""):
    """Opens a Carpet 0D ASCII datafile and reads its columns, returning a
    numpy array.

    PARAMETERS:
    ------------

    datafile: (string) the datafile path.
    columns:  (list of strings) a list of reductions names to be read. If
              given, columns 'iteration' and 'time' plus the requested
              columns will be returned. If it is a list containing a single
              empty string, columns 'iteration', 'time' and 'data' will be
              returned.

              Column names don't have to correspond exactly to the ones in the
              file: a string which occurs in the column name and uniquely
              identifies it is sufficient.

    RETURNS:
    ---------

    data: numpy.array of shape (N, 2 + len(names)). N is the number of rows in
          the file. The first two columns of the array contain the iteration
          number and time, respectively. The following columns are the ones
          specified in the 'columns' argument.
    """

    try:
        with open(datafile, 'r', newline='') as fo:

            # Read a bunch of lines at the start of the file
            opening = fo.readlines(8000)
            fo.seek(0, 0)

            # Set the data columns numbers
            column_names = parse_column_names(opening)
            if columns != "":
                tmp = []
                for column in columns:
                    for name in column_names:
                        if column in name:
                            tmp = tmp + [column_names[name]]
                            break
                columns = [column_names["it"],
                           column_names["time"]] + tmp
            else:
                columns = [column_names["it"],
                           column_names["time"],
                           column_names["data"]]

            # Figure out how many lines are there more or less
            num_lines = int(np.array([len(opening[i].encode("ascii"))
                                      for i in range(20)]).mean())
            num_lines = int(fo.seek(0, 2)/num_lines)*4
            data = [[]]*num_lines
            fo.seek(0, 0)

            # Setup the csv reader
            reader = csv.reader(fo, delimiter="\t")

            # Read the data
            i = 0
            for line in reader:
                # Jump over empty lines and comments
                if len(line) == 0:
                    continue
                if len(line) == 1:
                    continue
                if line[0][0] == '#':
                    continue

                data[i] = line

                i += 1

    except FileNotFoundError as excp:
        print(excp.strerror + ": " + excp.filename)
        exit()

    # Remove empty trailing lines
    data = data[:i]

    # For each line, get also space separated values,
    # flatten the line, cast to floats
    data = [[float(item) for sublist in
            [s if len(s) == 1 else s.split() for s in line]
            for item in sublist] for line in data]

    # Turn to np.array
    data = np.array(data)

    # Return the columns asked for
    return data[:, columns]
