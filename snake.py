# rugutils: a suite of minimalist Carpet data reading and plotting scripts
# Copyright (C) 2016, Federico Guercilena
# <guercilena@th.physik.uni-frankfurt.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import matplotlib.pyplot as plt
from matplotlib import animation
import matplotlib.gridspec as gs
import numpy as np
from read_hdf5 import read_file_structure, read_component
from itertools import repeat, cycle
import sys
from os import devnull
import expression_evaluator as ee
import h5py
from utils import parse_dataset_name


documentation = """
The snake command lets you plot 1D Carpet data in HDF5 format, then animate
it, showing a frame for every iteration available. It can handle multiple
files sampled at different times and of different length. It allows the
user to interact with the animation by handling keyboard events.

[OPTIONS]

'DATASETS(S)'
One or more strings of the type "varname1,varname2,...@datafile", where:

    varnames: A comma separated list of the names of the desired variables

    datafile: A path to one or more Carpet 1D HDF5 files.

The variable name is optional. In that case, the first variable found in the
file displayed. Also, variable names don't have to match exactly the ones
in the file (which could be very long): a string appearing in the variable
name and uniquely identifying it is sufficient.

'-R/--reflevel REFLEVEL'
If this option is present, only the refinement level specified in it will be
read and displayed.

'--bounds XMIN XMAX'
Fix the values of the extent of the plot on the x axis to the given
value. If this option and '--limits' is not given, the script will resize
the graph at every iteration to fit the current data.

'--limits YMIN YMAX'
Fix the values of the extent of the plot on the y axis to the given
value. If this option and '--bounds' is not given, the script will resize
the graph at every iteration to fit the current data.

'-r/--rate RATE'
The frame rate, i.e. the number of frames per second.

'--save-to FILENAME'
Don't display the animation, but directly save it to a rasterized file
called FILENAME.
The video file will have a bitrate of 400 kbits/s . For this option to work the
'ffmpeg' program must be installed on your system.

'--fig-size'
Two ints specifying the figure size in pixels along the x and y axis
respectively. Only relevant and available when saving to disk (with the
'--save-to' option). The default is 1280x720.

'-f/--expression EXPR'
This option allows the user to apply some mathematical expression to the data
before plotting. The expression EXPR will be evaluated at runtime and the
result will be applied to all input DATAFILES. In the EXPRESSION, 'y'
represents the values of the data on the ordinate (or y) axis. So for example:
    -f "y*3 + abs(y)"
will plot every data point multiplied by 3 and add its absolute value.
Common uses of the option are taking the log or the absolute value of the data,
but many more functions and expressions are supported, including array
indexing. For details and a list of functions available, type "rug doc
expression_evaluator".

'-q/--quiet'
Disable output messages (useful for scripts).

Note: There are further options added and this manual has not been kept in sync.
  Please refer to the arguments accepted by rug by calling "rug snake --help".

[KEYBOARD COMMANDS]
'spacebar'
Pause and resume the animation

'right/left arrows'
Skip ahead or behind in the animation by one single frame.
This works also when the animation is paused.

'up/down arrows'
Skip ahead or behind in the animation. The amount of time skipped is
hard coded to 1/20 of the total data length. This works also when
the animation is paused.

'ctrl+r'
Restart the animation, from the very first frame. If the animation
is paused, it will stay so.

'ctrl+e'
Skip to the end of the animation (last frame) and pause the animation
there.

'q'
Quit the program.
"""

__doc__ = documentation

def find_nearest(array, value):
    idx = (np.abs(array - value)).argmin()
    return idx


def snake(datasets, reflevel=None, rate=10, expr=None, limits=None,
          saveto=None, figsize=None, quiet=False, bounds=None,
          x_label=None, y_label=None, show_progress=True, y_scale="linear"):

    # Disable messages
    if quiet:
        sys.stdout = open(devnull, 'w')

    # Pause between frames in ms
    interval = 1000/rate

    # Set up the expression evaluator if needed
    if expr is not None:
        evaluator = ee.SimpleEval()

    # Loop over the datasets
    ncurves = 0
    for dataset in datasets:

        # Count how many curves there are
        names = parse_dataset_name(dataset)[0]
        ncurves += len(names)

    # Set up the figure and some other stuff
    fig = plt.figure()
    if figsize is not None:
        fig.set_size_inches(figsize[0]/96., figsize[1]/96.)
    else:
        fig.set_size_inches(1280./96., 720./96.)
    grid = gs.GridSpec(2, 1, height_ratios=[1, 20])

    rasterized = False
    if saveto is not None:
        rasterized = True

    ax = fig.add_subplot(grid[1, 0], rasterized=rasterized)
    ax.tick_params(which="major", reset=True, labelsize="x-large")
    if bounds is not None:
        ax.set_xlim([bounds[0], bounds[1]])
    if limits is not None:
        ax.set_ylim([limits[0], limits[1]])

    # Loop over the datasets
    i = 0
    t_initials = [[]]*ncurves
    t_finals = [[]]*ncurves
    delta_ts = [[]]*ncurves
    times = [[]]*ncurves
    iterations = [[]]*ncurves
    structs = [[]]*ncurves
    hfiles = [[]]*ncurves
    realnames = [[]]*ncurves
    lines = [[]]*ncurves
    for dataset in datasets:

        # Alert the user
        print("Scanning file", i + 1, "of", len(datasets), "...", end=" ",
              flush=True)

        # Get the names and file
        names, file = parse_dataset_name(dataset)

        # Loop over the names
        for name in names:

            # Create the empty line object
            lines[i] = ax.plot([], [], "s-", lw=3,
                               label=(name if name else "data")
                               + "@" + file)[0]

            # Create the h5py file object (read only)
            hfiles[i] = h5py.File(file, 'r')

            # Read the structure of the file
            realnames[i], structs[i] = read_file_structure(hfiles[i], name)

            # Get the iterations
            iterations[i] = sorted(structs[i])

            # Loop over iterations
            times[i] = [[]]*len(structs[i])
            j = 0
            for iteration in iterations[i]:

                if reflevel is not None:

                    # Check the wanted reflevel is available
                    if reflevel not in structs[i][iteration]:
                        print("Error! Refinement level", reflevel,
                              "not present at iteration", iteration,
                              "! Aborting...", flush=True)
                        sys.exit()

                    # Select reflevel as the one to use to extract times
                    a_reflevel = reflevel

                else:

                    # Select the coarsest
                    a_reflevel = 0

                times[i][j] = read_component(hfiles[i], realnames[i],
                                             iteration, a_reflevel,
                                             structs[i][iteration][a_reflevel]
                                             [0],
                                             metadata_only=True).time

                j += 1

            # Get the initial times, final times, delta time
            t_initials[i] = times[i][0]
            t_finals[i] = times[i][-1]
            delta_ts[i] = times[i][1] - t_initials[i]

            i += 1

        # Alert the user
        print("Done.", flush=True)

    print("Setting up the animation object...", flush=True)

    # Get the extent in time
    min_t = np.min(t_initials)
    max_t = np.max(t_finals)

    # Get the datafile with the finest time resolution
    dt = np.min(delta_ts)

    # Create the progress bar
    if show_progress:
        pbar_ax = fig.add_subplot(grid[0, 0], xlim=[min_t, max_t], ylim=[0, 1],
                                rasterized=rasterized)
        pbar_ax.set_yticks([])
        time = pbar_ax.text((max_t - min_t)/2 + min_t, 0.9,
                            "Time: {0:0f}".format(min_t),
                            va="top", ha="center", color="black", size="xx-large")
        time_line = pbar_ax.barh(0.02, 0, height=0.3, color="black")[0]

    # Print legend
    ax.legend(loc="upper right")

    # Print axes labels
    if x_label: ax.set_xlabel(x_label)
    if y_label: ax.set_ylabel(y_label)

    # Allow for log scale (actually, could allow even more here)
    # This is because calling rug snake -f 'log10(something)'
    # can render different then when pylab makes the log10 on the scale,
    # when it comes to values which are very close or exact 0.
    ax.set_yscale(y_scale)

    # Get the total number of frames
    max_frames = int((max_t - min_t)/dt)

    # The "empty" init function, needed for bliting
    def init():
        for line in lines:
            line.set_data([], [])
        if show_progress:
            time.set_text("Time: {0:0f}".format(0.0))
            time_line.set_width(0)
            return lines + [time, time_line]
        else:
            return lines

    # The animation function (draws a frame)
    def animate(n):
        now = min_t + dt*n
        for i in range(ncurves):
            lines[i].set_data([], [])
            # Get the frame nearest to the current time
            if ((now < t_initials[i]) or (now > t_finals[i])):
                # There's no data at this time, so don't plot this line
                continue

            # Figure out the iteration number
            iteration = iterations[i][find_nearest(times[i], now)]

            def get_iteration_data(iteration):
                # Loop over the refinement levels
                if reflevel is not None:
                    reflevel_list = [reflevel]
                else:
                    reflevel_list = structs[i][iteration]

                X, Y = [], []
                for rl in reflevel_list:
                    # Loop over the components
                    for c in structs[i][iteration][rl]:

                        # Read in the component
                        comp = read_component(hfiles[i], realnames[i],
                                            iteration, rl, c)

                        # Compute the grid points
                        # This is a tuple and in 1D we only need the first axis.
                        grid = comp.get_grid()[0]

                        assert grid.shape == comp.data.shape, "Mismatch in shape of read-in data"
                        X.append(grid)
                        Y.append(comp.data)

                # Merge the individual numpy coordinates from each reflevel
                # to a single 1d numpy array for X and Y
                assert len(X) and len(Y), "No data found in reflevels/components"
                X, Y = (np.concatenate(X), np.concatenate(Y))

                # sort points
                indices = np.argsort(X)
                X, Y = X[indices], Y[indices]

                return (X,Y)

            xdata, ydata = get_iteration_data(iteration)
            lines[i].set_data(xdata, ydata)

            # Apply an expression on the data, if needed
            if expr is not None:
                evaluator.names["y"] = ydata
                evaluator.functions["y_it"] = lambda it: get_iteration_data(it)[1] # return y values
                lines[i].set_ydata(evaluator.eval(expr))

        if bounds is None or limits is None:
            ax.relim()
            ax.autoscale_view()
        if saveto is not None:
            print("Done frame ", n + 1, "of", max_frames, flush=True)
        if show_progress:
            time.set_text("Time: {0:0f}".format(now))
            time_line.set_width(now)
            return lines + [time, time_line]
        else:
            return lines

    # Animation object
    anim = animation.FuncAnimation(fig, animate, init_func=init,
                                   frames=max_frames, interval=interval,
                                   blit=True, repeat=False)

    if saveto is not None:
        print("Saving to file...", end="", flush=True)
        anim.save(saveto, writer="ffmpeg", fps=rate, dpi=96,
                  codec="libx264", bitrate=400)
        print(" Done.", flush=True)
        sys.exit()

    print(" Done.", flush=True)

    # Pause, quit, restart, etc.,  the animation on key events (ugly)
    piece = max_frames//20
    pause = cycle([True, False])

    def onKey(event):
        if event.key == "q":
            sys.exit()
        elif event.key == " ":
            if pause.__next__():
                anim.frame_seq = repeat(next(anim.frame_seq, max_frames) - 1)
            else:
                anim.frame_seq = range(next(anim.frame_seq, max_frames),
                                       max_frames).__iter__()
        elif event.key == "up":
            if pause.__next__():
                anim.frame_seq = range(anim.frame_seq.__next__() + piece,
                                       max_frames).__iter__()
            else:
                anim.frame_seq = repeat(anim.frame_seq.__next__() + piece)
            pause.__next__()
        elif event.key == "down":
            if pause.__next__():
                anim.frame_seq = range(anim.frame_seq.__next__() - piece,
                                       max_frames).__iter__()
            else:
                anim.frame_seq = repeat(anim.frame_seq.__next__() - piece)
            pause.__next__()
        elif event.key == "right":
            if pause.__next__():
                anim.frame_seq = range(anim.frame_seq.__next__() + 1,
                                       max_frames).__iter__()
            else:
                anim.frame_seq = repeat(anim.frame_seq.__next__() + 1)
            pause.__next__()
        elif event.key == "left":
            if pause.__next__():
                anim.frame_seq = range(anim.frame_seq.__next__() - 1,
                                       max_frames).__iter__()
            else:
                anim.frame_seq = repeat(anim.frame_seq.__next__() - 1)
            pause.__next__()
        elif event.key == "ctrl+r":
            if pause.__next__():
                anim.frame_seq = range(0, max_frames).__iter__()
            else:
                anim.frame_seq = repeat(0)
            pause.__next__()
        elif event.key == "ctrl+e":
            anim.frame_seq = repeat(max_frames - 1)
            if not pause.__next__():
                pause.__next__()

    fig.canvas.mpl_connect('key_press_event', onKey)

    # Show the animation
    plt.show(anim)
