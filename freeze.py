# rugutils: a suite of minimalist Carpet data reading and plotting scripts
# Copyright (C) 2016, Federico Guercilena
# <guercilena@th.physik.uni-frankfurt.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import matplotlib.pyplot as plt
from read_hdf5 import read_file_structure, read_component
import expression_evaluator as ee
import numpy as np
import h5py
from os import devnull
import sys
from utils import parse_dataset_name


documentation = """
The freeze command lets you plot 1D Carpet data in HDF5 format at a given
iteration. The available arguments and options are:

'DATASETS(S)'
One or more strings of the type "varname1,varname2,...@datafile", where:

    varnames: A comma separated list of the names of the desired variables

    datafile: A path to one or more Carpet 1D HDF5 files.

The variable name is optional. In that case, the first variable found in the
file displayed. Also, variable names don't have to match exactly the ones
in the file (which could be very long): a string appearing in the variable
name and uniquely identifying it is sufficient.

'ITERATION'
Integer, required. The iteration number to read and display.

'-R/--reflevel REFLEVEL'
If this option is present, only the refinement level specified in it will be
read and displayed.

'-f/--expression EXPR'
This option allows the user to apply some mathematical expression to the data
before plotting. The expression EXPR will be evaluated at runtime and the
result will be applied to all input DATAFILES. In the EXPRESSION, 'y'
represents the values of the data on the ordinate (or y) axis. So for example:
    -f "y*3 + abs(y)"
will plot every data point multiplied by 3 and add its absolute value.
Common uses of the option are taking the log or the absolute value of the data,
but many more functions and expressions are supported, including array
indexing. For details and a list of functions available, type "rug doc
expression_evaluator".

'--save-to FILENAME'
Don't display the figure, but directly save it to a rasterized file
called FILENAME.

'--fig-size'
Two ints specifying the figure size in pixels along the x and y axis
respectively. Only relevant and available when saving to disk (with the
'--save-to' option). The default is 1280x720.

'-q/--quiet'
Disable output messages (useful for scripts).
"""

__doc__ = documentation


def update_line(line, new_data_x, new_data_y):
    line.set_xdata(np.append(line.get_xdata(), new_data_x))
    line.set_ydata(np.append(line.get_ydata(), new_data_y))


def sort_line(line):
    indices = np.argsort(line.get_xdata())
    line.set_xdata(line.get_xdata()[indices])
    line.set_ydata(line.get_ydata()[indices])


def freeze(datasets, iteration, reflevel=None, expr=None,
           quiet=False, saveto=None, figsize=None):

    # Disable messages
    if quiet:
        sys.stdout = open(devnull, 'w')

    # Set up the expression evaluator if needed
    if expr is not None:
        evaluator = ee.SimpleEval()

    # Set up the figure and axes
    fig = plt.figure()

    fig.set_dpi(96)
    if figsize is not None:
        fig.set_size_inches(figsize[0]/96., figsize[1]/96.)
    else:
        fig.set_size_inches(1280./96., 720./96.)

    rasterized = False
    if saveto is not None:
        rasterized = True
    ax = fig.add_subplot(111, rasterized=rasterized)

    # Loop over the datasets
    i = 0
    nfiles = len(datasets)
    for dataset in datasets:

        # Alert the user
        print("Reading file", i + 1, "of", nfiles, "...", end=" ",
              flush=True)

        # Get the variable and file names
        names, file = parse_dataset_name(dataset)

        # Create the h5py file object (read only)
        hfile = h5py.File(file, 'r')

        # Loop over names
        for name in names:

            # Create the empty line object
            line = ax.plot([], [], "s-", lw=3,
                           label=(name if name else "data") + "@" + file)[0]

            # Read the structure of the file
            realname, struct = read_file_structure(hfile, name)

            # Check the wanted iteration is available
            if iteration not in struct:
                print("Error! Iteration", iteration, "not in datafile!"
                      " Aborting...", flush=True)
                sys.exit()

            # Select the wanted iteration
            struct = struct[iteration]

            if reflevel is not None:

                # Check the wanted reflevel is available
                if reflevel not in struct:
                    print("Error! Refinement level", reflevel, "not present at"
                          " iteration", iteration, "! Aborting...", flush=True)
                    sys.exit()

                # Select the wanted reflevel
                reflevel_list = [reflevel]

            else:

                # Select all reflevels
                reflevel_list = struct

            # Loop over the refinement levels
            for rl in reflevel_list:

                # Loop over the components
                for c in struct[rl]:

                    # Read in the component
                    comp = read_component(hfile, realname,
                                          iteration, rl, c)

                    # Compute the grid points
                    grid = comp.get_grid()

                    # Add the data to the line object
                    update_line(line, grid, comp.data)

            # Sort points
            sort_line(line)

            # Apply an expression on the data, if needed
            if expr is not None:
                evaluator.names["y"] = line.get_ydata()
                line.set_ydata(evaluator.eval(expr))

        # Alert the user
        print("Done.", flush=True)
        i += 1

    # Adjust the axes
    ax.relim()
    ax.autoscale_view()

    # Set up the legend and ticks and title
    ax.legend(loc="best")
    ax.tick_params(which="major", reset=True, labelsize="x-large")
    ax.set_title("Time: " + str(comp.time), fontsize="x-large")

    # Show the plot or save it
    if saveto is not None:
        print("Saving to file...", end="", flush=True)
        fig.savefig(saveto, dpi=96)
        print(" Done.", flush=True)
        sys.exit()

    else:
        plt.show()
