# rugutils: a suite of minimalist Carpet data reading and plotting scripts
# Copyright (C) 2016, Federico Guercilena
# <guercilena@th.physik.uni-frankfurt.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter
from read_scalascii import read_scal_data
from read_0Dascii import read_0D_data
import expression_evaluator as ee
import sys
from os import devnull
from utils import parse_dataset_name


documentation = """
The rope command lets you plot scalar/0D Carpet data in ASCII format,
i.e. time series data, such as the norm1 reduction of some grid function over
time. The available arguments and options are:

'DATASET(S)'
One or more strings of the type "colname1,colname2,...@datafile", where:

    colnames: A comma separated list of the names of the desired columns

    datafile: A path to one or more Carpet scalar/0D ASCII files.

The column name is optional. In that case, the column named 'data' is
displayed. Also, column names don't have to match exactly the ones in the
file (which could be very long): a string appearing in the column name and
uniquely identifying it is sufficient.

'-f/--expression EXPR'
This option allows the user to apply some mathematical expression to the data
before plotting. The expression EXPR will be evaluated at runtime and the
result will be applied to all input DATAFILES. In the EXPRESSION, 'y'
represents the values of the data on the ordinate (or y) axis. So for example:
    -f "y*3 + abs(y)"
will plot every data point multiplied by 3 and add its absolute value.
Common uses of the option are taking the log or the absolute value of the data,
but many more functions and expressions are supported, including array
indexing. For details and a list of functions available, type "rug doc
expression_evaluator".

'--save-to FILENAME'
Don't display the figure, but directly save it to a rasterized file
called FILENAME.

'--fig-size'
Two ints specifying the figure size in pixels along the x and y axis
respectively. Only relevant and available when saving to disk (with the
'--save-to' option). The default is 1280x720.

'-q/--quiet'
Disable output messages (useful for scripts).
"""

__doc__ = documentation


def rope(datasets, expr=None, quiet=False, saveto=None, figsize=None):

    # Disable messages
    if quiet:
        sys.stdout = open(devnull, 'w')

    # N  of files
    nfiles = len(datasets)

    # Set up the expression evaluator if needed
    if expr is not None:
        evaluator = ee.SimpleEval()

    # Set up the figure
    fig = plt.figure()

    fig.set_dpi(96)
    if figsize is not None:
        fig.set_size_inches(figsize[0]/96., figsize[1]/96.)
    else:
        fig.set_size_inches(1280./96., 720./96.)

    rasterized = False
    if saveto is not None:
        rasterized = True
    ax = fig.add_subplot(111, rasterized=rasterized)

    # Set up the tick properties
    formatter = ScalarFormatter(useOffset=False)
    ax.xaxis.set_major_formatter(formatter)
    ax.yaxis.set_major_formatter(formatter)

    # Loop over the dtasets
    i = 0
    for dataset in datasets:

        # Alert user
        print("Reading file", i + 1, "of", nfiles, "...", end=" ",
              flush=True)

        # Get the columns and file names
        columns, file = parse_dataset_name(dataset)

        # Read the data
        if "scalars.asc" in file:
            data = read_scal_data(file, columns)
        elif "..asc" in file:
            data = read_0D_data(file, columns)
        elif ".asc" in file:
            data = read_scal_data(file)
        else:
            print("Error! File extension not recognised! It has to"
                  " be '*scalars.asc' or '*.asc'. Aborting...")
            sys.exit()

        # Apply an expression to the data if necessary
        if expr is not None:
            for col in range(2, len(columns) + 2):
                evaluator.names["y"] = data[:, col]
                data[:, col] = evaluator.eval(expr)

        # Plot the data
        for col in range(2, len(columns) + 2):
            ax.plot(data[:, 1], data[:, col], "-", lw=3,
                    label=(columns[col - 2] if columns[col - 2]
                           else "data") + "@" + file)

        # Alert the user
        print("Done.", flush=True)

        i += 1

    ax.tick_params(which="major", reset=True, labelsize="x-large")

    # Set up legend
    ax.legend(loc="best")

    # Show the plot or save it
    if saveto is not None:
        print("Saving to file...", end="", flush=True)
        fig.savefig(saveto, dpi=96)
        print(" Done.", flush=True)
        sys.exit()

    else:
        plt.show()
