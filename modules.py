# rugutils: a suite of minimalist Carpet data reading and plotting scripts
# Copyright (C) 2016, Federico Guercilena
# <guercilena@th.physik.uni-frankfurt.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from importlib import import_module
from inspect import signature

# List of available modules
module_list = ["rope", "freeze", "snake", "window", "cinema", "river"]

module_instances = None
module_functions = None

try:
    # Load the modules and the function they provide into dictionaries
    module_instances = {modname: import_module(modname)
                        for modname in module_list}
    module_functions = {modname: getattr(modinstance, modname)
                        for modname, modinstance in module_instances.items()}
except AttributeError as err:
    print("Broken module: Missing main function!")
    print(module_instances)
    raise AttributeError(err)


def call(given_args):
    """
    Use Python introspection to call the module named "command" with the
    command and options given in the arguments.
    """

    # Get the commend name and chek if it's a valid one
    # (argparse has already checked, so it should never fail)
    command = given_args.command
    if command not in module_functions:
        raise AttributeError(
                "There is no such module called {0}".format(command))

    # Transform the arguments into a dictionary
    # and remove the command name from it (so only the options remain)
    given_args = vars(given_args)
    del given_args["command"]

    # Get the function to call and its list of arguments
    function = module_functions[command]
    function_args = dict(signature(function).parameters)

    # Check that the arguments given and those required coincide
    if function_args.keys() != given_args.keys():
        print("Missing neccessary argument!")
        print("This is a bug in rugutils!")

    # Call the function with the given arguments
    return function(**given_args)
