# rugutils: a suite of minimalist Carpet data reading and plotting scripts
# Copyright (C) 2016, Federico Guercilena
# <guercilena@th.physik.uni-frankfurt.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import sys
import numpy as np
from re import search, compile


class component:
    """Class representing a single component in a 1D or 2d Carpet HDF5 file."""

    def __init__(self,
                 time=None,
                 timestep=None,
                 level=None,
                 origin=None,
                 delta=None,
                 n=None,
                 nghosts=None,
                 data=None):
        self.time = time
        self.timestep = timestep
        self.level = level
        self.origin = origin
        self.delta = delta
        self.n = n
        self.nghosts = nghosts
        self.data = data
        if self.n is None:
            self.end = None
        else:
            self.end = np.array([self.origin[i] + self.delta[i]*(self.n[i] - 1)
                                 for i in range(len(origin))])

    def get_grid(self):
        """Return the grid points associated with the component.

        RETURNS:
        ---------

        grid: tuple of numpy arrays. In 1D, the tuple has only one element
              (the x coordinates of the grid points). In 2D, the tuple has two
              elements, the x and y coordinates of the grid.
        """

        grid = tuple(
                np.linspace(self.origin[i],
                            self.end[i],
                            self.n[i]) for i in range(self.data.ndim))

        return grid


def read_component(hfile, name, it, rl, c,
                   metadata_only=False, no_ghosts=False):
    """Read a given component from a 1D or 2D HDF5 Carpet file.

    PARAMETERS:
    ------------

    hfile:         (h5py.File object) the object pointing to the file on disc.
    name:          (string) the name of the variable to be read, or a substring
                   uniquely identifying it.
    it:            (int) iteration number.
    rl:            (int) refinament level number.
    c:             (int) component number.
    metadata_only: (bool) if true, only read the metadata of the component, but
                   not the actual data values (default: False).
    no_ghosts:     (bool) if true, discard the ghost points from the data
                   array. The number of points and the grid origin are updated
                   accordingly, while nghosts is set to zero. Only makes sense
                   if metadata_only=False (default: False).

    RETURNS:
    ---------

    realname: a string containing the full name of the variable as saved in
              the file.
    struct:   a dictionary describing the file structure. Keys are the
              iteration numbers present in the file. Every value is another
              dictionary, in which the keys are the refinement level numbers
              present at that iteration. The values are lists of the components
              numbers.
    """

    if c >= 0:
        string = "{0:s} it={1:d} tl=0 rl={2:d} c={3:d}".format(name, it, rl, c)
    else:
        string = "{0:s} it={1:d} tl=0 rl={2:d}".format(name, it, rl)
    attrs = hfile[string].attrs
    if metadata_only:
        data = None
    else:
        data = hfile[string].value

    comp = component(attrs["time"],
                     it,
                     rl,
                     attrs["origin"],
                     attrs["delta"],
                     list(data.shape[::-1]) if data is not None else None,
                     attrs["cctk_nghostzones"],
                     data)

    if no_ghosts and not metadata_only:
        inner = []
        for i in range(comp.data.ndim):
            comp.origin[i] = comp.origin[i] + comp.delta[i]*comp.nghosts[i]
            comp.end[i] = comp.end[i] - comp.delta[i]*comp.nghosts[i]
            comp.n[i] = comp.n[i] - comp.nghosts[i]*2
            inner = inner + [slice(comp.nghosts[i], -comp.nghosts[i])]
            comp.nghosts[i] = 0
        comp.data = comp.data[inner]

    return comp


def read_file_structure(hfile, name=""):
    """Read the structure of a Carpet 1D or 2D HDF5 file.

    PARAMETERS:
    ------------

    hfile:         (h5py.File object) the object pointing to the file on disc.
    name:          (string) the name of the variable to be read, or a substring
                   uniquely identifying it. If an empty string, the first
                   variable found in the file is read.

    RETURNS:
    ---------

    realname:  a string containing the full name of the variable as saved in
               the file.
    component: the component object.
    """

    name_matchstring = compile("(.+) it=") # i.e. "it is a dataset"
    struct_matchstring = compile("it=(?P<it>[0-9]+) tl=0 rl=(?P<rl>[0-9]{1,2})(?: c=(?P<c>[0-9]+))?")
    struct = {}

    if name:
        realname = None
        for dataset in hfile:
            if name in dataset:
                realname = search(name_matchstring, dataset).group(1)
                break

        if realname is None:
            print("Error! No variable containing '" + str(name) + "' in its"
                  " name in the given file! Aborting...", flush=True)
            sys.exit()
    else:
        # had to remove the "decode" call
        try:
                realname = hfile["Parameters and Global Attributes/Datasets"].value[0].decode('unicode_escape')
        except AttributeError:
                # most likely, value is of type numpy.bytes, i.e. there is only one Dataset.
                realname = hfile["Parameters and Global Attributes/Datasets"].value.decode('unicode_escape')
        realname = realname.strip() # remove trailing newline (Cactus writes it)

    #print("Realname is '%s'" % str(realname))
    for dataset in hfile:
        if realname in dataset:
            match = search(struct_matchstring, dataset)
            dataset_params = match.groupdict()
            it = int(dataset_params["it"])
            rl = int(dataset_params["rl"])
            if dataset_params["c"] is not None:
                c = int(dataset_params["c"])
            else:
                c = -1 # kind of "illegal value"
            if it not in struct:
                struct[it] = {}
                struct[it][rl] = [c]
            else:
                if rl not in struct[it]:
                    struct[it][rl] = [c]
                else:
                    struct[it][rl] = struct[it][rl] + [c]
																																																		
    return realname, struct

